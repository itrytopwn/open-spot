package observis.leetgames.com.mikkeliopenspot.utilities.data;

import com.google.android.gms.maps.model.LatLng;


import java.io.Serializable;

public class ParkingLots  implements Serializable {

    int parking_ID;
    MyLatLng entryCoord;
    MyLatLng cornerLeftUpper;
    MyLatLng cornerRightUpper;
    MyLatLng cornerLeftLower;
    MyLatLng cornerRightLower;
    ParkTime paidStart = null;
    ParkTime paidEnd = null;
    ParkTime parkTime = null;
    String descr;

    public ParkingLots(){

    }

    public ParkingLots(int id, LatLng ent){
        this.parking_ID = id;

        this.entryCoord = new MyLatLng(ent.latitude,ent.longitude);
    }

    public ParkingLots(int id, LatLng ent,ParkTime ps,ParkTime pe,ParkTime pt){
        this.parking_ID = id;
        this.entryCoord = new MyLatLng(ent.latitude,ent.longitude);
        this.paidStart = ps;
        this.paidEnd = pe;
        this.parkTime = pt;
    }

    public ParkingLots(int id, LatLng ent, LatLng clu, LatLng cru, LatLng cll, LatLng crl){
        this.parking_ID = id;
        this.entryCoord = new MyLatLng(ent.latitude,ent.longitude);
        this.cornerLeftUpper = new MyLatLng(clu.latitude,clu.longitude);
        this.cornerRightUpper = new MyLatLng(cru.latitude,cru.longitude);
        this.cornerLeftLower = new MyLatLng(cll.latitude,cll.longitude);
        this.cornerRightLower = new MyLatLng(crl.latitude,crl.longitude);



    }

    public ParkingLots(int id,LatLng ent, LatLng clu, LatLng cru, LatLng cll, LatLng crl, ParkTime ps,  ParkTime pe, ParkTime pt){
        this.parking_ID = id;
        this.entryCoord = new MyLatLng(ent.latitude,ent.longitude);
        this.cornerLeftUpper = new MyLatLng(clu.latitude,clu.longitude);
        this.cornerRightUpper = new MyLatLng(cru.latitude,cru.longitude);
        this.cornerLeftLower = new MyLatLng(cll.latitude,cll.longitude);
        this.cornerRightLower = new MyLatLng(crl.latitude,crl.longitude);

        this.paidStart = ps;
        this.paidEnd = pe;
        this.parkTime = pt;

    }
    public void addDescription(String d){
        this.descr = d;

    }

    public LatLng getEntryCoord() {
        return new LatLng(this.entryCoord.getLatitude(),this.entryCoord.getLongitude());
    }

    public void setEntryCoord(LatLng ent) {
        this.entryCoord = new MyLatLng(ent.latitude,ent.longitude);
    }


    public LatLng getCornerLeftUpper() {
        return new LatLng(this.cornerLeftUpper.getLatitude(),this.cornerLeftUpper.getLongitude());
    }


    public void setCornerLeftUpper(LatLng clu) {
        this.cornerLeftUpper = new MyLatLng(clu.latitude,clu.longitude);

    }

    public LatLng getCornerRightUpper() {
        return new LatLng(this.cornerRightUpper.getLatitude(),this.cornerRightUpper.getLongitude());
    }

    public void setCornerRightUpper(LatLng cru) {
        this.cornerRightUpper = new MyLatLng(cru.latitude,cru.longitude);
    }

    public LatLng getCornerRightLower() {
        return new LatLng(this.cornerRightLower.getLatitude(),this.cornerRightLower.getLongitude());
    }

    public void setCornerRightLower(LatLng crl) {
        this.cornerRightLower =  new MyLatLng(crl.latitude,crl.longitude);
    }

    public LatLng getCornerLeftLower() {
        return new LatLng(this.cornerLeftLower.getLatitude(),this.cornerLeftLower.getLongitude());
    }

    public void setCornerLeftLower(LatLng cll) {
        this.cornerLeftLower =  new MyLatLng(cll.latitude,cll.longitude);
    }

    public ParkTime getPaidStart() {
        return this.paidStart;
    }

    public ParkTime getPaidEnd() {
        return this.paidEnd;
    }

    public ParkTime getParkTime() {
        return this.parkTime;
    }

    public static class MyLatLng implements Serializable {

        double latitude;

        double longitude;

        public MyLatLng( ) {

        }
        public MyLatLng( double lat,double lon) {
            this.latitude = lat;
            this.longitude = lon;
        }

        public double getLatitude() {
            return this.latitude;
        }

        public double getLongitude() {
            return this.longitude;
        }
    }

    public static class ParkTime implements Serializable {
        int hour;
        int minute;


        public ParkTime(){

        }
        public ParkTime(int h, int m){
            this.hour = h;
            this.minute = m;
        }
        public int getMinutes(){
            int tmp = (this.hour * 60) + this.minute;
            return tmp;
        }
        public String stringWithDivider(){
            return ("" + String.format("%02d",this.hour) + ":" + String.format("%02d", this.minute));
        }

        public String stringWithLetters(){
            return ("" + String.format("%02d",this.hour) + "h " + String.format("%02d", this.minute) + "m");
        }

    }

}