package observis.leetgames.com.mikkeliopenspot.utilities;

import java.util.ArrayList;
import java.util.Iterator;

import observis.leetgames.com.mikkeliopenspot.utilities.data.ParkingLots;

/**
 * Created by itryt on 05.09.2016.
 */
public class TimeFilter {

    public static ArrayList<ParkingLots> parkDurationFilter(ArrayList<ParkingLots> in, int reqTime) {
        ArrayList<ParkingLots> n = (ArrayList<ParkingLots>) in.clone();



        for (Iterator<ParkingLots> it = n.iterator(); it.hasNext(); ) {
            ParkingLots aParkingLot = it.next();
            if (aParkingLot.getParkTime() != null && aParkingLot.getParkTime().getMinutes() < reqTime) {
                it.remove();
            }
        }

        return (ArrayList<ParkingLots>) n.clone();
    }
}

//            for (ParkingLots entry : n) {
//                if (entry.getParkTime() != null && entry.getParkTime().getMinutes() < reqTime) {
//                    n.remove(entry);
//                }
//            }