package observis.leetgames.com.mikkeliopenspot.utilities.ihelpers;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import observis.leetgames.com.mikkeliopenspot.GetDistance;
import observis.leetgames.com.mikkeliopenspot.R;
import observis.leetgames.com.mikkeliopenspot.utilities.data.ParkData;
import observis.leetgames.com.mikkeliopenspot.utilities.data.ParkingLots;

public class LowPageFiller extends AsyncTask<Void, Void, Void> {
    ArrayList<ParkingLots> parkingArr;
    private Activity act;
    private View rootView;
    private LatLng a;
    private LatLng b;

    public LowPageFiller(Activity activity, View rv, ArrayList<ParkingLots> arrtmp, LatLng at, LatLng bt) {
        act = activity;
        rootView = rv;
        parkingArr = arrtmp;
        a = at;
        b = bt;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {


        Geocoder geocoder = new Geocoder(act, Locale.getDefault());
        GetDistance g = new GetDistance();
        final String dist = g.getDistanceText(a, b);

        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(b.latitude, b.longitude, 5);

        } catch (IOException ioException) {
            // Catch network or other I/O problems.

        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
        }

        final String address = addresses.get(0).getAddressLine(0);

        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ParkingLots node = ParkData.getObjectByEntry(b, parkingArr);
                rootView.findViewById(R.id.fragment_loader).setVisibility(View.GONE);
                //                 rootView.findViewById(R.id.fragment_data).setVisibility(View.VISIBLE);
                LinearLayout l = (LinearLayout) rootView.findViewById(R.id.fragment_data);
                l.setVisibility(View.VISIBLE);
                rootView.findViewById(R.id.directions_button).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri gmmIntentUri = Uri.parse("google.navigation:q="+b.latitude+","+b.longitude);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        act.startActivity(mapIntent);
                    }
                });
                ((TextView) rootView.findViewById(R.id.pager_address_text)).setText(address);
                ((TextView) rootView.findViewById(R.id.pager_distance_text)).setText(dist);
                rootView.requestLayout();

                if (node.getPaidStart() == null) {
                    ((TextView) rootView.findViewById(R.id.pager_time_text)).setText("FREE");
                } else {
                    String t = node.getPaidStart().stringWithDivider() + " - " + node.getPaidEnd().stringWithDivider();
                    ((TextView) rootView.findViewById(R.id.pager_time_text)).setText(t);
                }

                if (node.getPaidStart() == null) {
                    ((TextView) rootView.findViewById(R.id.pager_length_text)).setText("UNLIMITED");
                } else {
                    ((TextView) rootView.findViewById(R.id.pager_length_text)).setText(node.getParkTime().stringWithLetters());
                }
//
//                            ((TextView) rootView.findViewById(R.id.pager_content_text)).setText((args.getString(ARG_OBJECT)));

            }
        });


        return null;
    }

    @Override
    protected void onPostExecute(Void v) {
        super.onPostExecute(v);
//        ViewPager p = (ViewPager) act.findViewById(R.id.pager);
//        if(p.getCurrentItem() == 0){
//            p.setCurrentItem(0);
//        }
    }
}

