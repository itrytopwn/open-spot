package observis.leetgames.com.mikkeliopenspot.utilities.gps;

import android.location.Location;

/**
 * Created by itryt on 05-Aug-16.
 */
public interface  LocationTracker {
     interface LocationUpdateListener{
         void onUpdate(Location oldLoc, long oldTime, Location newLoc, long newTime);
    }

     void start();
     void start(LocationUpdateListener update);

     void stop();

     boolean hasLocation();

     boolean hasPossiblyStaleLocation();

     Location getLocation();

     Location getPossiblyStaleLocation();
}
