package observis.leetgames.com.mikkeliopenspot;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.widget.EditText;
import android.widget.Toast;

import com.aigestudio.wheelpicker.WheelPicker;
import com.google.android.gms.maps.model.LatLng;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import mbanje.kurt.fabbutton.FabButton;
import observis.leetgames.com.mikkeliopenspot.activities.MapsActivity;
import observis.leetgames.com.mikkeliopenspot.utilities.TimeFilter;
import observis.leetgames.com.mikkeliopenspot.utilities.data.ParkData;
import observis.leetgames.com.mikkeliopenspot.utilities.data.ParkingLots;
import observis.leetgames.com.mikkeliopenspot.utilities.gps.FallbackLocationTracker;

public class Loader  {


    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 234;

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 312;
    private static final int MY_PERMISSIONS_REQUEST_INTERNET = 42;
    private FabButton button;
    private FallbackLocationTracker flt;

    public Loader(Activity a,int procKey) {
        Load l = new Load(a, procKey);
        l.execute();
    }

    public class Load extends AsyncTask<String, Void, Intent> {
        private Activity activity;
        private int k;
        private LatLng first;
        private LatLng target;
        private LatLng second;

        public ArrayList<ParkingLots> fArray = (ArrayList<ParkingLots>) ParkData.parkingLotsArrayList.clone();
        public Load(Activity a, int f) {
            flt = new FallbackLocationTracker(a);

            this.activity = a;
            k = f;
           button = (FabButton) activity.findViewById(R.id.spinningbutton );
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            flt.start();
            WheelPicker wh1 = (WheelPicker) activity.findViewById(R.id.wheel_view_1);
            String selectedOnWheel = wh1.getData().get(wh1.getCurrentItemPosition()).toString();
            Scanner sc = new Scanner(selectedOnWheel);
            if (sc.hasNextInt()){
                int test = sc.nextInt();
                fArray = TimeFilter.parkDurationFilter(fArray,test);
            }
        }



        @Override
        protected Intent doInBackground(String... params) {
            first = determineFirst(k,activity);
            target = determineTarget(k,activity);
            if(first == null || target ==null){
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        button.showProgress(false);
                        CharSequence text = "Location is unavailable";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(activity, text, duration);
                        toast.show();
                    }
                });
                this.cancel(true);

            }
            second = determineSecond(target);

            Intent i = new Intent(activity, MapsActivity.class);
            Bundle extra = new Bundle();

            extra.putString("firstPointData", first.latitude + "," + first.longitude);
            extra.putString("closestParkData", second.latitude + "," + second.longitude);

            extra.putSerializable("parkArray",fArray);
            i.putExtra("extra",extra);
            return i;
        }

        @Override
        protected void onPostExecute(Intent i) {
            button.showProgress(false);
            flt.stop();
            activity.startActivity(i);



        }
    }



    private LatLng determineFirst(int flag,Activity a) {
        LatLng result = null;

        if (flag == 0 | flag == 2) {
        result = getCurrentLocation();
        }
        else{
            if(flag == 1) {
                result = decodeAddress(a);
            }
        }


        return result;
    }

    private LatLng determineSecond(LatLng target) {

        GetDistance g = new GetDistance();
        ParkingLots closest = ParkData.parkingLotsArrayList.get(0);
        double closestpath = 0;
        closestpath = g.getDistanceLength(target, ParkData.parkingLotsArrayList.get(0).getEntryCoord());
        for(ParkingLots l : ParkData.parkingLotsArrayList){
            if (closestpath > g.getDistanceLength(target, l.getEntryCoord())) {
                closestpath = g.getDistanceLength(target, l.getEntryCoord());
                closest = l;
            }
        }
        String s =  g.getDistanceText(target,closest.getEntryCoord());
        return closest.getEntryCoord();
    }

    private LatLng determineTarget(int flag,Activity a) {
        LatLng result = null;

        if (flag == 0 ) {
            result = getCurrentLocation();
        }
        else{
            if(flag == 1 | flag == 2) {
                result = decodeAddress(a);
            }
        }


        return result;
    }




    private LatLng getCurrentLocation() {
        LatLng result = null;
        if(flt.hasLocation()){
            result = new LatLng(flt.getLocation().getLatitude(),flt.getLocation().getLongitude());
        }else{

            try {
                Thread.sleep(2000);
                result = getCurrentLocation();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return result;
    }




    private LatLng decodeAddress(Activity activity){
        EditText t = (EditText) activity.findViewById(R.id.editTextAddress);

        String a =t.getText().toString();
        LatLng decoded;
        decoded = null;
        Geocoder coder = new Geocoder(activity);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(a, 5, 61.501734, 26.903089, 61.849022, 27.678999);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();


            decoded = new LatLng(location.getLatitude(), location.getLongitude());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return decoded;
    }



}
