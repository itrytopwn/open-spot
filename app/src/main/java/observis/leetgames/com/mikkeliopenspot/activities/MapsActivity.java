package observis.leetgames.com.mikkeliopenspot.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;


import java.io.Console;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mbanje.kurt.fabbutton.FabButton;
import observis.leetgames.com.mikkeliopenspot.GetDistance;
import observis.leetgames.com.mikkeliopenspot.pager.LowPager;
import observis.leetgames.com.mikkeliopenspot.utilities.ihelpers.CircularReveal;
import observis.leetgames.com.mikkeliopenspot.utilities.drawing.DrawParking;
import observis.leetgames.com.mikkeliopenspot.utilities.data.ParkData;
import observis.leetgames.com.mikkeliopenspot.utilities.data.ParkingLots;
import observis.leetgames.com.mikkeliopenspot.R;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 234;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 312;
    private static final int MY_PERMISSIONS_REQUEST_INTERNET = 42;
    private GoogleMap mMap;
    Bundle extras;
    private View rootLayout;
    private View mapView;
    public Bundle savedInstance;
    private LowPager.ParkNodePagerAdapter parkNodePagerAdapter;
    private ViewPager mViewPager;
    private ArrayList<ParkingLots> sortedArr;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        savedInstance = savedInstanceState;
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.transition.do_not_move, R.transition.do_not_move);

        setContentView(R.layout.activity_maps);
        rootLayout = findViewById(R.id.root_layout);
        rootLayout.setVisibility(View.INVISIBLE);
        this.runOnUiThread(new Runnable() {
            public void run() {
                if (rootLayout.getViewTreeObserver().isAlive()) {
                    rootLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @SuppressWarnings("deprecation")
                        // We use the new method when supported
                        @SuppressLint("NewApi")
                        // We check which build version we are using.
                        @Override
                        public void onGlobalLayout() {
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                rootLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            } else {
                                rootLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            }
                            CircularReveal.circularRevealActivity(rootLayout, 100);
                        }
                    });
                }
            }
        });

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapView = mapFragment.getView();


        mapFragment.getMapAsync(this);


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2 = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Maps Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://observis.leetgames.com.mikkeliopenspot/http/host/path")
        );
        AppIndex.AppIndexApi.start(client2, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Maps Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://observis.leetgames.com.mikkeliopenspot/http/host/path")
        );
        AppIndex.AppIndexApi.end(client2, viewAction);
        client2.disconnect();
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        overridePendingTransition(R.transition.do_not_move, R.transition.do_not_move);
//        CircularReveal.circularHideActivity(mapView);
//
//    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Bundle extras = getIntent().getBundleExtra("extra");

        LatLng first;
        LatLng second;


        sortedArr = (ArrayList<ParkingLots>) extras.getSerializable("parkArray");

        String[] divideList = extras.getString("firstPointData").split(",");
        first = new LatLng(Double.parseDouble(divideList[0]), Double.parseDouble(divideList[1]));

        divideList = extras.getString("closestParkData").split(",");
        second = new LatLng(Double.parseDouble(divideList[0]), Double.parseDouble(divideList[1]));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(61.681263, 27.2554154), 11.73f));
        parkNodePagerAdapter = new LowPager.ParkNodePagerAdapter(getSupportFragmentManager(),sortedArr , mMap, first);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(parkNodePagerAdapter);
        mViewPager.setOffscreenPageLimit(4);
        mViewPager.addOnPageChangeListener(parkNodePagerAdapter.l);

        final ImageButton larr = (ImageButton) findViewById(R.id.left_pager_arrow);
        final ImageButton rarr = (ImageButton) findViewById(R.id.right_pager_arrow);
        larr.setVisibility(View.INVISIBLE);

        rarr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int t = mViewPager.getCurrentItem();
                if(t != mViewPager.getAdapter().getCount() - 1)
                    mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
            }
        });

        larr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int t = mViewPager.getCurrentItem();
                if(t != 0)
                    mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {



                super.onPageSelected(position);
                if(position == 0){
                    larr.setVisibility(View.INVISIBLE);
                    rarr.setVisibility(View.VISIBLE);
                }else {
                    if (position == sortedArr.size() - 1){
                        rarr.setVisibility(View.INVISIBLE);
                        larr.setVisibility(View.VISIBLE);
                    }else {
                        larr.setVisibility(View.VISIBLE);
                        rarr.setVisibility(View.VISIBLE);
                    }
                }

            }
        });



        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);

        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Provide an additional rationale to the user if the permission was not granted
                // and the user would benefit from additional context for the use of the permission.
                // For example if the user has previously denied the permission.

            } else {

                // Camera permission has not been granted yet. Request it directly.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        }
        mMap.setMyLocationEnabled(true);


    }

}

