package observis.leetgames.com.mikkeliopenspot.utilities.ihelpers;

import android.app.Activity;
import android.view.View;

import java.util.ArrayList;

import observis.leetgames.com.mikkeliopenspot.R;

public class InterfaceClear {

    public static void execute(Activity host ,boolean show){
        final Activity a = host;
        ArrayList<View> elements = new ArrayList<View>(){{
            add(a.findViewById(R.id.input_layout_password));
            add(a.findViewById(R.id.routing_switch));
            add(a.findViewById(R.id.descLabel1));
            add(a.findViewById(R.id.textView));

        }
        };

        if (show) {
            for (View elem : elements){
                viewVisibility(elem,View.VISIBLE);
            }
        }else{
            for (View elem : elements){
                viewVisibility(elem,View.GONE);
            }

        }


    }

    static private void viewVisibility(View v,int visibility){
        v.setVisibility(visibility);

    }
}
