package observis.leetgames.com.mikkeliopenspot.utilities.drawing;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import observis.leetgames.com.mikkeliopenspot.utilities.data.JSONParser;
import observis.leetgames.com.mikkeliopenspot.utilities.data.ParkData;
import observis.leetgames.com.mikkeliopenspot.utilities.data.ParkingLots;

public class Drawing
{
    private GoogleMap mMap;
    public int lastDrawDistance;
    public String lastDrawDistanceShort;
    private LatLngBounds.Builder builder = new LatLngBounds.Builder();

    public Drawing(GoogleMap m) {
    mMap = m;

    }

    public void connectPoints(LatLng a, LatLng b){
    //JSONParser pars = new JSONParser();
  //  String json = pars.getJSONFromUrl(url);
        new drawTask(a,b).execute();

}


    private String makeURL (LatLng first, LatLng second ){
        double sourcelat = first.latitude ;
        double sourcelog = first.longitude ;
        double destlat = second.latitude ;
        double destlog = second.longitude ;
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(sourcelat));
        urlString.append(",");
        urlString
                .append(Double.toString( sourcelog));
        urlString.append("&destination=");// to
        urlString
                .append(Double.toString( destlat));
        urlString.append(",");
        urlString.append(Double.toString( destlog));
        urlString.append("&sensor=false&mode=driving&alternatives=true");
        urlString.append("&key=AIzaSyBbg_NKsrP_kFMfxcW6vK27ECc7Ne_D23o");
        return urlString.toString();
    }


    private class drawTask extends AsyncTask<Void, Void, PolylineOptions> {
        private ProgressDialog progressDialog;
        String url;
        LatLng a;
        LatLng b;
        drawTask(LatLng at,LatLng bt ) {
            a = at;
            b = bt;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected PolylineOptions doInBackground(Void... params) {
            String url = makeURL(a,b);
            JSONParser jParser = new JSONParser();
            String json = jParser.getJSONFromUrl(url);
            return buildPolyline(json);
        }

        @Override
        protected void onPostExecute(PolylineOptions polyopt) {
            super.onPostExecute(polyopt);
            if (polyopt != null) {
                drawPath(polyopt);
            }
        }

        public void drawPath(PolylineOptions resultopt) {


            mMap.clear();
            mMap.addPolyline(resultopt);

            mMap.addMarker(new MarkerOptions()
                    //.anchor(0.0f, 1.0f) // Anchors the marker on the bottom left
                    .position(resultopt.getPoints().get(resultopt.getPoints().size()-1))
                    .icon(BitmapDescriptorFactory.fromAsset("1p.png")));
            drawParkOutline(b);
            LatLngBounds bounds = builder.build();


            int padding = 150;

            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds,padding));
           /*
           for(int z = 0; z<list.size()-1;z++){
                LatLng src= list.get(z);
                LatLng dest= list.get(z+1);
                Polyline line = mMap.addPolyline(new PolylineOptions()
                .add(new LatLng(src.latitude, src.longitude), new LatLng(dest.latitude,   dest.longitude))
                .width(2)
                .color(Color.BLUE).geodesic(true));
            }
           */

        }


        public void drawParkOutline(LatLng a){
            ParkingLots lot = ParkData.getObjectByEntry(a);
            if(lot.getCornerLeftUpper() != null) {
                mMap.addPolygon(new PolygonOptions()
                        .add(lot.getCornerLeftUpper(), lot.getCornerRightUpper(), lot.getCornerRightLower(), lot.getCornerLeftLower())
                        .strokeColor(Color.argb(150, 255, 255, 255))
                        .fillColor(Color.argb(150, 0, 0, 0)));
            }
        }
    }

    private PolylineOptions buildPolyline(String result) {
        PolylineOptions options = new PolylineOptions().width(12).color(Color.argb(200,0,179,253)).geodesic(true);
        try {
            //Tranform the string into a json object
            final JSONObject json = new JSONObject(result);
            JSONArray routeArray = json.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
            List<LatLng> list = decodePoly(encodedString);

            lastDrawDistance = routes.getJSONArray("legs").getJSONObject(0).getJSONObject("distance").getInt("value");
            lastDrawDistanceShort = routes.getJSONArray("legs").getJSONObject(0).getJSONObject("distance").getString("text");

            for (int z = 0; z < list.size(); z++) {
                LatLng point = list.get(z);

                options.add(point);
                builder.include(point);
            }

           /*
           for(int z = 0; z<list.size()-1;z++){
                LatLng src= list.get(z);
                LatLng dest= list.get(z+1);
                Polyline line = mMap.addPolyline(new PolylineOptions()
                .add(new LatLng(src.latitude, src.longitude), new LatLng(dest.latitude,   dest.longitude))
                .width(2)
                .color(Color.BLUE).geodesic(true));
            }
           */
        } catch (JSONException e) {

        }
        return options;
    }




    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }













}

