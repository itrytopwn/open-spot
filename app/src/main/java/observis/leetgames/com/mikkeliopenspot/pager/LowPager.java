package observis.leetgames.com.mikkeliopenspot.pager;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import observis.leetgames.com.mikkeliopenspot.R;
import observis.leetgames.com.mikkeliopenspot.utilities.data.ParkData;
import observis.leetgames.com.mikkeliopenspot.utilities.data.ParkingLots;
import observis.leetgames.com.mikkeliopenspot.utilities.drawing.DrawParking;
import observis.leetgames.com.mikkeliopenspot.utilities.ihelpers.LowPageFiller;

/**
 * Created by itryt on 07-Aug-16.
 */
public class LowPager {
    public static class LowPagerObjectFragment extends Fragment {
        public static final String ARG_OBJECT = "object";
        public static final String NUM = "numeration";
        private boolean instantiated;
        public static ParkNodePagerAdapter m;
        private LatLng p;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            ViewPager mPager = (ViewPager) container.findViewById(R.id.pager);
            m = (ParkNodePagerAdapter) mPager.getAdapter();

            View rootView = inflater.inflate(
                    R.layout.pager_fragment, container, false);
            Bundle args = getArguments();

            String[] divideList = args.getString(ARG_OBJECT).split(",");
            this.p = new LatLng(Double.parseDouble(divideList[0]), Double.parseDouble(divideList[1]));

            LowPageFiller k = new LowPageFiller(getActivity(),rootView,m.arr,m.f,this.p);
            k.execute();

            return rootView;
        }



    }




    public static class ParkNodePagerAdapter extends FragmentStatePagerAdapter {
        private ArrayList<ParkingLots> arr;
        public GoogleMap mMap;
        public LatLng f;
        public ViewPager.SimpleOnPageChangeListener l;
        public ParkNodePagerAdapter(FragmentManager fm, ArrayList<ParkingLots> a, GoogleMap m, LatLng first) {
            super(fm);
            arr = a;
            mMap = m;
            f = first;
            l = new ViewPager.SimpleOnPageChangeListener(){
                @Override
                public void onPageSelected(int position) {
                    super.onPageSelected(position);
                    ParkingLots pl = arr.get(position);
                    Log.d("CHANGED", "FRAGMENT CHANGED TO " + position);
                    mMap.clear();
                    DrawParking dp = new DrawParking(mMap,arr,pl.getEntryCoord());
                    dp.moveCamera(f,pl.getEntryCoord());

                }
            };
            DrawParking dp = new DrawParking(mMap,arr,arr.get(0).getEntryCoord());
            dp.moveCamera(f,arr.get(0).getEntryCoord());

        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = new LowPagerObjectFragment();
            Bundle args = new Bundle();
            // Our object is just an integer :-P
            LatLng tmp = arr.get(i).getEntryCoord();
            args.putString(LowPagerObjectFragment.ARG_OBJECT, arr.get(i).getEntryCoord().latitude + "," + arr.get(i).getEntryCoord().longitude);
            args.putString(LowPagerObjectFragment.NUM, arr.get(i).getEntryCoord().latitude + "," + arr.get(i).getEntryCoord().longitude);

            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return arr.size();
        }


    }
}
