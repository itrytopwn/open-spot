package observis.leetgames.com.mikkeliopenspot;


import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import observis.leetgames.com.mikkeliopenspot.utilities.data.JSONParser;

public class GetDistance {

    public double getDistanceLength(LatLng a, LatLng b){
        LatLng first = a;
        LatLng second = b;
        String url = makeURL(first,second);
        String k = null;
        //JSONParser pars = new JSONParser();
        //  String json = pars.getJSONFromUrl(url);

         k = calc(url);


        return getPathLength(k);

    }

    public String getDistanceText(LatLng a, LatLng b){
        LatLng first = a;
        LatLng second = b;
        String url = makeURL(first,second);
        String k = null;
        //JSONParser pars = new JSONParser();
        //  String json = pars.getJSONFromUrl(url);

        k = calc(url);


        return getPathShort(k);

    }

    private String calc(String url){
        JSONParser jParser = new JSONParser();
        String json = jParser.getJSONFromUrl(url);
        return json;
    }

    public String getPathShort(String result){
        String distance = null;

        try {
            //Tranform the string into a json object

            JSONObject json = new JSONObject(result);
            JSONArray routeArray = json.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");

            distance = routes.getJSONArray("legs").getJSONObject(0).getJSONObject("distance").getString("text");
            distance += " " + routes.getJSONArray("legs").getJSONObject(0).getJSONObject("duration").getString("text");

        } catch (JSONException e) {

        }
        return distance;
    }

    public double getPathLength(String result){
        double distance = 0;

        try {
            //Tranform the string into a json object

            JSONObject json = new JSONObject(result);
            JSONArray routeArray = json.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");

            distance = routes.getJSONArray("legs").getJSONObject(0).getJSONObject("distance").getInt("value");


        } catch (JSONException e) {

        }
        return distance;
    }

    private String makeURL (LatLng first, LatLng second ){
        double sourcelat = first.latitude ;
        double sourcelog = first.longitude ;
        double destlat = second.latitude ;
        double destlog = second.longitude ;
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(sourcelat));
        urlString.append(",");
        urlString
                .append(Double.toString( sourcelog));
        urlString.append("&destination=");// to
        urlString
                .append(Double.toString( destlat));
        urlString.append(",");
        urlString.append(Double.toString( destlog));
        urlString.append("&sensor=false&mode=driving&alternatives=true");
        urlString.append("&key=AIzaSyBbg_NKsrP_kFMfxcW6vK27ECc7Ne_D23o");
        return urlString.toString();
    }


}
