package observis.leetgames.com.mikkeliopenspot.utilities.ihelpers;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import observis.leetgames.com.mikkeliopenspot.R;

/**
 * Created by itryt on 05-Aug-16.
 */
public class CircularReveal {
    public static void circularRevealActivity(View v,int x) {


        int cx = (v.getLeft() + v.getRight()) / 2;
        int cy = Math.round((v.getTop() + v.getBottom()) / 3);

        int dx = Math.max(cx, v.getWidth() - cx);
        int dy = Math.max(cy, v.getHeight() - cy);
        float finalRadius = (float) Math.hypot(dx, dy);

        // create the animator for this view (the start radius is zero)
        Animator circularReveal = ViewAnimationUtils.createCircularReveal(v, cx, cy, 125, finalRadius);
        circularReveal.setInterpolator(new AccelerateDecelerateInterpolator());
        circularReveal.setDuration(300);

        // make the view visible and start the animation
        v.setVisibility(View.VISIBLE);

        circularReveal.start();

    }

    public static void circularHideActivity(View v) {


        int cx = (v.getLeft() + v.getRight()) / 2;
        int cy = (int) Math.round((v.getTop() + v.getBottom()) / 2.7);

        int dx = Math.max(cx, v.getWidth() - cx);
        int dy = Math.max(cy, v.getHeight() - cy);
        float finalRadius = (float) Math.hypot(dx, dy);

        // create the animator for this view (the start radius is zero)
        Animator circularReveal = ViewAnimationUtils.createCircularReveal(v, cx, cy, finalRadius, 0);
        circularReveal.setInterpolator(new AccelerateDecelerateInterpolator());
        circularReveal.setDuration(300);

        // make the view visible and start the animation
        v.setVisibility(View.VISIBLE);
        circularReveal.start();
    }
}
