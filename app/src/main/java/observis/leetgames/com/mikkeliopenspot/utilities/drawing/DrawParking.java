package observis.leetgames.com.mikkeliopenspot.utilities.drawing;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import java.util.ArrayList;

import observis.leetgames.com.mikkeliopenspot.utilities.data.JSONParser;
import observis.leetgames.com.mikkeliopenspot.utilities.data.ParkData;
import observis.leetgames.com.mikkeliopenspot.utilities.data.ParkingLots;

/**
 * Created by itryt on 06-Aug-16.
 */
public class DrawParking {
    private GoogleMap mMap;
    private ArrayList<ParkingLots> mArr;
    private LatLng entryCoord;
    public DrawParking(GoogleMap m,ArrayList<ParkingLots> ar,LatLng e){
        mMap = m;
        mArr = ar;
        entryCoord = e;
    }


    public void moveCamera(LatLng f, LatLng s) {

        if (f != null && s != null) {


            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(f);
            builder.include(s);

            LatLngBounds bounds = builder.build();

            int padding = 100;

            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));

          /*  CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(f)      // Sets the center of the map to location user
                    .zoom(15)                   // Sets the zoom
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/
            Drawing draw = new Drawing(mMap);
            draw.connectPoints(f, s);


        }

    }

    private class ParkingRect extends AsyncTask<Void, Void, Void> {
        private GoogleMap mMap;
        ParkingRect(GoogleMap m,ArrayList<ParkingLots> ar,LatLng e) {
            mMap = m;
            mArr = ar;
            entryCoord = e;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            super.onPostExecute(v);


        }
    }
}
