package observis.leetgames.com.mikkeliopenspot.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.aigestudio.wheelpicker.WheelPicker;

import java.io.Console;
import java.util.Arrays;
import java.util.List;

import mbanje.kurt.fabbutton.FabButton;
import observis.leetgames.com.mikkeliopenspot.utilities.ihelpers.InterfaceClear;
import observis.leetgames.com.mikkeliopenspot.Loader;
import observis.leetgames.com.mikkeliopenspot.utilities.ihelpers.ProgressHelper;
import observis.leetgames.com.mikkeliopenspot.R;


public class MainActivity extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 234;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 312;
    private static final int MY_PERMISSIONS_REQUEST_INTERNET = 42;
    private static final List<String> WHEEL_VALUES_TIME = Arrays.asList("Any time","15 min", "30 min", "45 min","60 min","90 min","120 min");

    private FabButton button;
    private ProgressHelper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (FabButton) findViewById(R.id.spinningbutton );
        helper = new ProgressHelper(button,this);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.time_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner


        final FabButton button = (FabButton) findViewById(R.id.spinningbutton );

        if(button!=null) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openMap(v);
                }
            });
        }

        if (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

        }
        else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Provide an additional rationale to the user if the permission was not granted
                // and the user would benefit from additional context for the use of the permission.
                // For example if the user has previously denied the permission.

            } else {

                // Camera permission has not been granted yet. Request it directly.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        }


        WheelPicker wh1 = (WheelPicker) findViewById(R.id.wheel_view_1);
        wh1.setData(WHEEL_VALUES_TIME);



        WheelPicker.OnItemSelectedListener mWheelListener = new WheelPicker.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {
                Log.v("WHEELY", "Data is: " + data.toString());

            }
        };

        WheelPicker.OnWheelChangeListener mWheelChangeListener = new WheelPicker.OnWheelChangeListener() {
            @Override
            public void onWheelScrolled(int offset) {
                int tmp = Math.abs(offset % 360);
                Log.v("WHEELY", "State is: " + tmp);
                if(tmp < 120){

                }
                else{
                    if(tmp < 240){

                    }

                    else{
                        if(tmp < 360){

                        }

                    }
                }

            }

            @Override
            public void onWheelSelected(int position) {

            }

            @Override
            public void onWheelScrollStateChanged(int state) {

            }
        };
        wh1.setOnItemSelectedListener(mWheelListener);
        wh1.setOnWheelChangeListener(mWheelChangeListener);

    }


    @Override
    protected void onResume()
    {
        super.onResume();
        InterfaceClear.execute(this,true);
        button.showProgress(false);

    }
      public void openMap(View view) {
        // Do something in response to button
          startLoading();
          EditText t = (EditText) findViewById(R.id.editTextAddress);
          startLoading();
          if (t.getText().toString().equals("")) {
                  Loader l = new Loader(this,0);
              } else {
                  Switch s = (Switch) findViewById(R.id.routing_switch);

                  if(s.isChecked()) {
                      Loader l = new Loader(this,2);

                  } else {
                      Loader l = new Loader(this,1);
                  }
              }


          }



    private void startLoading(){
        helper.startIndeterminate(); //Button spinner start
        InterfaceClear.execute(this,false);
    }









}

