package observis.leetgames.com.mikkeliopenspot.utilities.data;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.ArrayList;

public class ParkData {
    public static ArrayList<ParkingLots> parkingLotsArrayList = new ArrayList<ParkingLots>() {{
        add(new ParkingLots(1, new LatLng(61.687716, 27.262072), new LatLng(61.688303, 27.262516),
                new LatLng(61.688121, 27.263889), new LatLng(61.687832, 27.262343), new LatLng(61.687678, 27.263568)));
        add(new ParkingLots(2, new LatLng(61.680301, 27.258556), new LatLng(61.681221, 27.258045),
                new LatLng(61.681475, 27.259405), new LatLng(61.680573, 27.258588), new LatLng(61.680818, 27.259998)));
        add(new ParkingLots(3, new LatLng(61.686397, 27.276664), new LatLng(61.686788, 27.276742),
                new LatLng(61.686797, 27.276902), new LatLng(61.686475, 27.276728), new LatLng(61.686448, 27.276809),new ParkingLots.ParkTime(8, 0),new ParkingLots.ParkTime(18, 0),new ParkingLots.ParkTime(0, 30)));
        add(new ParkingLots(4, new LatLng(61.688496, 27.267545), new ParkingLots.ParkTime(10, 0), new ParkingLots.ParkTime(16, 0), new ParkingLots.ParkTime(2, 0)));
    }};

    public ParkData() {

    }

    public static ParkingLots getObjectByEntry(LatLng t) {
        ParkingLots result = parkingLotsArrayList.get(0);
        for (ParkingLots p : parkingLotsArrayList) {
            if (p.getEntryCoord() != null) {
                if (p.entryCoord.equals(t)) {
                    result = p;
                }
            }
        }

        return result;

    }

    public static ParkingLots getObjectByEntry(LatLng t, ArrayList<ParkingLots> tmpArr) {
        ParkingLots result = tmpArr.get(0);
        for (ParkingLots p : tmpArr) {
            if (p.getEntryCoord() != null) {
                if (p.getEntryCoord().equals(t)) {
                    return p;
                }
            }
        }

        return result;

    }

}

